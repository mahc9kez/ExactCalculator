## ExactCalculator

This repo contains Gradle and GitLab CI config to build ExactCalculator the AOSP Calculator App.

**ExactCalculator** [refs/heads/pie-release](https://android.googlesource.com/platform/packages/apps/ExactCalculator/+/b1a9847d7a15762ff421ab4cff5caebf5a895257) and **crcalc** [refs/heads/android10-release](https://android.googlesource.com/platform/external/crcalc/+/ce5df439d984b6458f33d6d2fb30dfb0b6fa0011) are used as Git submodule.

To build the App with GitLab CI, you need to encode related values to BASE64 and set the following environment variables in GitLab Settings - CI/CI - Variables

```
CI_ANDROID_KEYSTORE
CI_ANDROID_STOREPASS
CI_PRIVATE_TOKEN
```
